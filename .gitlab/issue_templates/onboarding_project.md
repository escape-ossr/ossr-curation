# Onboarding project

_If you consider adding your repository to the OSSR, please fill the following template with the core information about your project. Minimal information would be **title** and a **link to the repository**_

## Project overview

**Title:** Write here the title of your project

**Description (Abstract):**

**Project Owner / Organization:**

## Repositories

_Add here a subsection for each repository intended for onboarding and that you want to upload as separate entry. 
Repeat for several repositories._

### (SUB)PROJECT TITLE

| Information | header |
| ------ | ------ |
| Project name | Please give a descriptive name |
| Repository link | URL |
| Project Owner | Name/Organization |

- [ ] Requested to add your Zenodo record to the `escape2020` community (and a merge request has been generated in this repository)

Note: When applying for community membership on Zenodo, you accept the [Terms and Conditions](https://escape-ossr.gitlab.io/ossr-pages/page/contribute/guidelines_ossr/)