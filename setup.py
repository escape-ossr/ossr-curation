#!/usr/bin/env python

import re
from setuptools import setup, find_packages
from pathlib import Path


entry_points = {'console_scripts': []}


def get_property(prop, project):
    result = re.search(r'{}\s*=\s*[\'"]([^\'"]*)[\'"]'.format(prop),
                       open(project + '/__init__.py').read())
    return result.group(1)


this_directory = Path(__file__).parent
long_description = (this_directory / "README.md").read_text()


setup(
    name='ossr_curation',
    version=get_property('__version__', 'ossr_curation'),
    description="OSSR Open Curation",
    long_description=long_description,
    long_description_content_type='text/markdown',
    install_requires=[
        "eossr>=2.1.0",
        "python-gitlab>=2.10.1",
        "BeautifulSoup4",
    ],
    packages=find_packages(),
    scripts=[],
    extras_require={'tests': ['pytest']},
    author='Thomas Vuillaume',
    author_email='vuillaume@lapp.in2p3.fr',
    url='https://gitlab.com/escape-ossr/ossr-curation',
    license='MIT',
    entry_points=entry_points,
    package_data={}
)
