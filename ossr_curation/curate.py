import os
import csv
from copy import deepcopy
import warnings
from pathlib import Path
import gitlab

from eossr.metadata import codemeta
from eossr.api.zenodo import ZenodoAPI, query_deposit
from eossr.api.zenodo.http_status import HTTPStatusError
from eossr.api.zenodo import Record

import tempfile


class CurateFile:
    fieldnames = ['conceptrecid', 'doi', 'html-link', 'status']
    delimiter = '\t'

    def __init__(self, filename, overwrite=False):
        self.filename = filename
        if not os.path.exists(self.filename) or overwrite:
            with open(self.filename, 'w') as file:
                writer = csv.DictWriter(file, fieldnames=CurateFile.fieldnames, delimiter=CurateFile.delimiter)
                writer.writeheader()

    def read(self):
        with open(self.filename, 'r') as file:
            reader = csv.DictReader(file, fieldnames=CurateFile.fieldnames, delimiter=CurateFile.delimiter)
            rows = [row for row in reader]
        return rows

    @property
    def rows(self):
        return self.read()

    def write(self, rows):
        for ii, row in enumerate(rows):
            if set(row.keys() != CurateFile.fieldnames):
                raise ValueError(f"row {ii}: {row} does not contain the requested keys {CurateFile.fieldnames}")

        with open(self.filename, 'w') as file:
            writer = csv.DictWriter(file, fieldnames=CurateFile.fieldnames, delimiter=CurateFile.delimiter)
            writer.writeheader()
            writer.writerows(rows)

    def accept_record(self, record_conceptrecid):
        rows = deepcopy(self.rows)
        for row in rows:
            if row['conceptrecid'] == record_conceptrecid:
                if row['status'] == 1:
                    raise ValueError(f"Record {record_conceptrecid} status is already 1")
                else:
                    row['status'] = 1
                    break  # there should be only one record with this ID, no need to continue looping
        self.write(rows)

    @property
    def concept_recids(self):
        if os.path.exists(self.filename):
            curate_dict = self.read()
            concept_recids = [int(rec['conceptrecid']) for rec in curate_dict[1:]]
        else:
            concept_recids = []
        return concept_recids

    @property
    def dois(self):
        if os.path.exists(self.filename):
            curate_dict = self.read()
            dois = [rec['doi'] for rec in curate_dict[1:]]
        else:
            dois = []
        return dois

    def add_record(self, conceptrecid):

        record = Record.from_id(conceptrecid)
        doi = record.doi
        html_link = record.data['links']['self_html']

        if conceptrecid in self.concept_recids:
            warnings.warn(f"Record {conceptrecid} "
                          f"is already in the registered records in {self.filename}",
                          stacklevel=2,
                          )
        else:
            with open(self.filename, 'a', newline='') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=CurateFile.fieldnames, delimiter=CurateFile.delimiter)
                writer.writerow({
                    'conceptrecid': conceptrecid,
                    'doi': doi,
                    'html-link': html_link,
                    'status': 1
                })


def validate_codemeta(record):
    """
    Validate the codemeta and return a message with the number of warnings and errors to deal with

    :param record:
    :return: str
    """
    try:
        codemeta_dict = record.get_codemeta()
    except FileNotFoundError:
        return f"\n----\n**!! There is no codemeta file in record {record.id} !!**\n"

    errors = ""

    codemeta_handler = codemeta.Codemeta(codemeta_dict)

    recommended = list(codemeta_handler.missing_keys('recommended'))
    required = list(codemeta_handler.missing_keys('required'))

    msg = ""

    # both lists are empty
    if not (recommended or required):
        msg += f'\n----\nGreat! All recommended and required keys are present.\n'

    if recommended:
        msg += f'\n----\nThere are {len(recommended)} missing recommended keys:{recommended}\n'

    if required:
        msg += f'\n----\n**There are {len(required)} missing required keys:{required}**\n' \
           f'{errors}'

    print(msg)
    return msg


def open_merge_request_for_curation(project_id, gitlab_token, request, curated_filename,
                                    gitlab_url="https://gitlab.com"):
    """
    Open a merge request after curated_filename has been updated.
        - creates a new branch with name == record.conceptrecid
        - adds modified `curated_filename` file to the branch
        - open merge request from that branch
        - use records info and checklist as merge request description

    :param project_id: int
        gitlab project ID
    :param gitlab_token: str
    :param record: `eossr.api.zenodo.Record`
    :param curated_filename: str
    :param gitlab_url: str
    """
    if not Path(curated_filename).exists():
        raise FileNotFoundError(f"{curated_filename} not found")

    record = request.record
    record_conceptrecid = record.data['conceptrecid']

    descrp = f"=== Record #{record.id} ===     \n"
    descrp += f"Title: {record.title}     \n"
    descrp += f"DOI: {record.data['doi']}     \n"
    descrp += f"request URL: {request.url}     \n"
    descrp += "\n\n"


    if record.data['metadata']['resource_type']['type'] in ['software', 'dataset']:
        checklist_path = Path(__file__).parent.joinpath('checklist.md')
        if not checklist_path.exists():
            raise FileNotFoundError("Checklist file not found")

        checklist = open(checklist_path).read()
        descrp += checklist

    descrp += validate_codemeta(record)

    gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_token)
    project = gl.projects.get(project_id)
    existing_branches_names = get_existing_branches_names(project_id, gitlab_token=gitlab_token)
    branch_name = str(record_conceptrecid)

    if branch_name in existing_branches_names:
        warnings.warn((f"Branch {branch_name} aready exists - curation process on-going?"))

    else:
        print(f"Creating branch {branch_name}")

        project.branches.create({'branch': branch_name, 'ref': 'master'})
        _, tmpfilename = tempfile.mkstemp()
        with open(tmpfilename, 'w') as tmpfile:
            tmpfile.write(open(curated_filename).read())
        tmpcuratefile = CurateFile(tmpfilename)
        tmpcuratefile.add_record(record_conceptrecid)

        project.commits.create({'branch': branch_name,
                                'commit_message': f'[BOT] Update {Path(curated_filename).name}',
                                'actions': [{'action': 'update',
                                             'file_path': curated_filename,
                                             'content': open(tmpfilename).read()}]
                                })

        mr = project.mergerequests.create({'source_branch': branch_name,
                                           'target_branch': 'master',
                                           'title': f'[CURATE] {record.title}',
                                           'labels': ['Ready for curation'],
                                           })
        mr.description = descrp
        mr.save()
        message = f"Hello. <br> \
        Thank you for your request to be added in the OSSR. <br> \
        Merge request {mr.id} has been opened to review record {record.id}. \
        Visit <a href={mr.web_url}>{mr.web_url}</a> and add a comment there to follow the review process. <br>\
        The OSSR team"
        response = request.post_message(message)
        if response:
            print(f"The following message has been posted to Zenodo:\n{message}")


def get_existing_branches_names(project_id, gitlab_token=None, gitlab_url="https://gitlab.com"):
    gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_token)
    project = gl.projects.get(project_id)
    existing_branches_names = [branch.name for branch in project.branches.list(all=True)]
    return existing_branches_names


if __name__ == '__main__':

    from config import *

    zen = ZenodoAPI(access_token=zenodo_token, sandbox=sandbox)
    pending_requests = zen.get_community_pending_requests(zenodo_community_name, size=100)

    def get_conceptrecid(pending_request):
        rec_id = pending_request.data['topic']['record']
        print(rec_id)
        try:
            record = pending_request.record
            recid = record.data['conceptrecid']
        except HTTPStatusError:
            deposit = query_deposit(rec_id, access_token=zenodo_token, sandbox=sandbox).json()
            recid = deposit['conceptrecid']
        return recid

    conceptredids = [get_conceptrecid(req) for req in pending_requests]

    curate_file = CurateFile(curated_filename)

    existing_branches_names = get_existing_branches_names(project_id, gitlab_token=GITLAB_TOKEN, gitlab_url=GITLAB_URL)

    for conceptrecid, request in zip(conceptredids, pending_requests):
        if int(conceptrecid) not in curate_file.concept_recids and conceptrecid not in existing_branches_names:
            print(f"New deposit with conceptrecid {conceptrecid}")
            print(request)
            print("\n\n")
            curate_file.add_record(conceptrecid)
            open_merge_request_for_curation(project_id, GITLAB_TOKEN, request, curated_filename)
