"""
Manually triggers a merge request for a record to be curated.
"""


import argparse

from eossr.api.zenodo import get_record
from ossr_curation.config import *
from ossr_curation.curate import open_merge_request_for_curation


# an argparse argument parser for this script
parser = argparse.ArgumentParser(description='Curate a record from its record ID')
parser.add_argument('record_id', type=int, help='the record ID to curate')

if __name__ == '__main__':
    # parse the command line arguments
    args = parser.parse_args()
    record_id = args.record_id
    # get the record from the Zenodo API
    record = get_record(record_id)
    record.print_info()
    open_merge_request_for_curation(project_id, GITLAB_TOKEN, record, curated_filename)

