# a script to curate a record from its record ID

import argparse

from eossr.api.zenodo import get_record
from ossr_curation.curate import validate_codemeta

# an argparse argument parser for this script
parser = argparse.ArgumentParser(description='Curate a record from its record ID')
parser.add_argument('record_id', type=int, help='the record ID to curate')

if __name__ == '__main__':
    # parse the command line arguments
    args = parser.parse_args()
    record_id = args.record_id
    # get the record from the Zenodo API
    record = get_record(record_id)
    record.print_info()
    msg = validate_codemeta(record)
    print(msg)

