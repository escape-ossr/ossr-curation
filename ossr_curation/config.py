import os


curated_filename = 'records/curated.csv'
project_id = 46887020  # to change when creating a new gitlab repository
GITLAB_URL = "https://gitlab.com/"
GITLAB_TOKEN = os.getenv('CI_JOB_TOKEN') if os.getenv('CI_JOB_TOKEN') is not None else os.getenv('GITLAB_TOKEN')
zenodo_community_name = 'escape2020'
zenodo_token=os.getenv('ZENODO_TOKEN')
sandbox = False

if GITLAB_TOKEN is None:
    raise ValueError("GITLAB_TOKEN not found")
