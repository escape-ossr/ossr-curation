## How to process this merge request

See also the [wiki](https://gitlab.com/escape-ossr/ossr-curation/-/wikis/Onboarding-process/) for more info.

- [ ] Check that an automated message has been posted in Zenodo's discussion for that request.
    - If not, write a message pointing to this merge request and inviting the record owner to join the merge request by commenting it.
- [ ] Check if a related onboarding issue exists and close onboarding issue.
- [ ] Resolve all open issues, then clear for merge.
- [ ] When also the presentation was given at a community event, close the request.

## Check the software checklist for the entry before accepting to community

- [ ] Contains valid codemeta.json (see validator output)
- [ ] Documentation is provided in the Zenodo entry (at least through codemeta)
- [ ] a stable versioned release of the project
- [ ] It is under an open-source license (see SPDX [https://spdx.org/licenses/])
- [ ] Follows a reasonable set of software development / software engineering practices (rough by-eye quality estimate)

## Add to community

- [ ] Presentation was given at OSSR community event.

