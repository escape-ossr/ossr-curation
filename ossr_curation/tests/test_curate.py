from ossr_curation import curate
from eossr.api.zenodo import ZenodoAPI
from eossr.api.zenodo.http_status import HTTPStatusError
from ossr_curation.config import zenodo_token, GITLAB_TOKEN
import pytest
import warnings


@pytest.fixture()
def init_curate_file(tmp_path):
    filename = tmp_path / 'curated.csv'
    curate_file = curate.CurateFile(filename)
    return curate_file


def test_init_curate_file(init_curate_file):
    tmpfile = init_curate_file.filename
    assert open(tmpfile).read() == \
        f'conceptrecid{init_curate_file.delimiter}doi{init_curate_file.delimiter}html-link{init_curate_file.delimiter}status\n'


def test_curate_file_add_record(init_curate_file):
    record_conceptrecid = 5524912
    init_curate_file.add_record(record_conceptrecid)
    assert record_conceptrecid in init_curate_file.concept_recids


def test_curate_sandbox():
    import os
    if GITLAB_TOKEN is None:
        raise ValueError("GITLAB_TOKEN not found")

    curated_filename = 'records/curated.csv'

    zen = ZenodoAPI(access_token=zenodo_token)
    pending_requests = zen.get_community_pending_requests('escape2020')
    records = []
    for req in pending_requests:
        try:
            records.append(req.record)
        except HTTPStatusError:
            warnings.warn(f"Record {req.record_id} not found or accessible. Skipping.")

    curate_file = curate.CurateFile(curated_filename)
    print(f"Curate file status before:\n{curate_file.read()}")

    for rec in records:
        print(rec)
        conceptrecid = rec.data['conceptrecid']
        if conceptrecid not in curate_file.concept_recids:
            rec.print_info()
            curate_file.add_record(conceptrecid)

    print(f"Curate file status after:\n{curate_file.read()}")
