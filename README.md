# ESCAPE OSSR requests curation

Open OSSR curation process for records to be accepted in the [ESCAPE OSSR community](https://zenodo.org/communities/escape2020). Applying for adding a record to the Zenodo community triggers a merge request here which is followed up to confirm quality standards.

The technical implementation uses GitLab merge requests to automatise the curation and acceptation in the OSSR. If you want to add your repository to the ESCAPE OSSR community but have no Zenodo entry yet, you can raise an issue here of the type Onboarding!

The guidelines and checklist for a record to be added to the OSSR are there:

http://purl.org/escape/ossr

A description of the curation process can be found in the [internal onboarding guideline](https://gitlab.com/escape-ossr/ossr-curation/-/wikis/Onboarding-process) in the Wiki.


## Curators

| Name | handle |
| ------ | ------ |
| Thomas Vuillaume | @vuillaut |
| Jutta Schnabel | @YouSchnabel |
| Enrique Garcia | @garciagenrique |
| Tamàs Gal | @tamas.gal |
| Christian Tacke | @c.tacke |
| Mark Kettenis | @kettenis |
| Kay Graf | @kay.graf |
